﻿module Deferred

type Deferred<'A> =
    | Resolved of 'A
    | Pending of (unit -> Deferred<'A>)

let rec resolve d =
    match d with
    | Resolved c -> c
    | Pending c -> resolve (c ())
