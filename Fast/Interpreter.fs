﻿module Interpreter

open Operation
open Expression
open Function
open Deferred

type Strategy = Eager | Lazy

let execute st op =
    let rec execute op pos top va =
        match top, Array.get op pos with
        | true, F (x, y) -> 
            Resolved { Apply = fun z -> Map.add x z va |> execute op y top
                       Expression = fexp (itos x) (Map.remove x va |> execute op y top |> resolve |> expression) }
        | true, A (x, y) ->
            match st with
            | Eager -> execute op x top va |> resolve |> apply (execute op y top va)
            | Lazy -> Pending (fun () -> execute op x top va |> resolve |> apply (execute op y top va))
        | true, S (x, _) ->
            let rec unbound e = 
                Resolved { Apply = fun x -> unbound (aexp e (x |> resolve |> expression))
                           Expression = e }
            Map.tryFind x va |> Option.defaultValue (unbound (sexp (itos x)))
        | false, T (x, _) ->
            execute op x (not top) va
        | _, _ -> 
            execute op (pos + 1) top va
    execute op 0 false Map.empty |> resolve