﻿module Compiler

open Operation
open Expression

exception InvalidOperationException of string

let transform e =
    let rec transform e =
        match e with
        | ListExp [SymbolExp "F"; SymbolExp x; ListExp y] ->
            fexp "k" (aexp (sexp "k") (fexp x (transform (ListExp y))))
        | ListExp [SymbolExp "S"; SymbolExp x] ->
            fexp "k" (aexp (sexp "k") (sexp x))
        | ListExp [SymbolExp "A"; ListExp x; ListExp y] ->
            fexp "k" (aexp (transform (ListExp x)) (fexp "m" (aexp (transform (ListExp y)) (fexp "n" (aexp (aexp (sexp "m") (sexp "n")) (sexp "k"))))))
        | _ -> raise (toString e |> InvalidOperationException)
    aexp (transform e) (fexp "x" (sexp "x"))

let compile e =
    let rec compile e i =
        match e with
        | ListExp [SymbolExp "F"; SymbolExp x; ListExp y] ->
            let (j, t) = compile (ListExp y) i
            (j + 1, t @ [F(stoi x, j - 1)])
        | ListExp [SymbolExp "A"; ListExp x; ListExp y] ->
            let (j, t) = compile (ListExp x) i
            let (k, u) = compile (ListExp y) j
            (k + 1, t @ u @ [A(j - 1, k - 1)])
        | ListExp [SymbolExp "S"; SymbolExp x] ->
            (i + 1, [S(stoi x, 0)])
        | _ -> raise (toString e |> InvalidOperationException)
    let (i, t) = compile e 0
    Array.ofList (t @ [T(i - 1, 0)])