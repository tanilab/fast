﻿module Operation

type Operation = 
| F of int * int
| A of int * int
| S of int * int
| T of int * int

let stoi (s: string) =
    Array.create 8 (byte 0)
    |> Array.append (System.Text.Encoding.ASCII.GetBytes s)
    |> System.ReadOnlySpan
    |> System.BitConverter.ToInt64
    |> int

let itos (n: int) =
    System.BitConverter.GetBytes n 
    |> System.Text.Encoding.ASCII.GetString

let toString op =
    let f o =
        match o with
        | F(a, b) -> Printf.sprintf "%08x %08x %08x" (stoi "F") a b
        | A(a, b) -> Printf.sprintf "%08x %08x %08x" (stoi "A") a b
        | S(a, b) -> Printf.sprintf "%08x %08x %08x" (stoi "S") a b
        | T(a, b) -> Printf.sprintf "%08x %08x %08x" (stoi "T") a b
    Array.map f op
    |> String.concat "\n"
