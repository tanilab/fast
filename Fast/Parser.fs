﻿module Parser

open FParsec.Primitives
open FParsec.CharParsers
open Expression

exception InvalidSyntaxException of string

let expression, expressionRef = createParserForwardedToRef<Expression, unit>()
do expressionRef :=
    (regex "[a-zA-Z]+" |>> SymbolExp) 
    <|> between (pchar '(') (pchar ')') (sepBy expression spaces1 |>> ListExp)

let parse input = 
    match run (spaces >>. expression .>> spaces .>> eof) input with
    | Success (r, _, _) -> r
    | Failure (r, _, _) -> raise (InvalidSyntaxException r)